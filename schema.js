const axios = require('axios');

const { GraphQLObjectType, GraphQLInt, GraphQLString, GraphQLBoolean, GraphQLList, GraphQLSchema } = require('graphql');

// Launch Type
const LaunchType = new GraphQLObjectType({
  name: 'Launch',
  fields: () => ({
    flightNumber: { type: GraphQLInt, resolve: data => data.flight_number },
    missionName: { type: GraphQLString, resolve: data => data.mission_name },
    launchYear: { type: GraphQLString, resolve: data => data.launch_year },
    launchDateLocal: {
      type: GraphQLString,
      resolve: data => data.launch_date_local,
    },
    isSuccess: { type: GraphQLBoolean, resolve: data => data.launch_success },
    rocket: { type: RocketType },
  }),
});

const RocketType = new GraphQLObjectType({
  name: 'Rocket',
  fields: () => ({
    rocketId: { type: GraphQLString, resolve: data => data.rocket_id },
    rocketName: { type: GraphQLString, resolve: data => data.rocket_name },
    rocketType: { type: GraphQLString, resolve: data => data.rocket_type },
  }),
});

// Root Query
const RootQuery = new GraphQLObjectType({
  name: 'RootQueryType',
  fields: {
    launches: {
      type: new GraphQLList(LaunchType),
      resolve(parent, args) {
        return axios.get('https://api.spacexdata.com/v3/launches').then(res => res.data);
      },
    },
    launch: {
      type: LaunchType,
      args: {
        flightNumber: {
          type: GraphQLInt,
        },
      },
      resolve(parent, args) {
        return axios.get(`https://api.spacexdata.com/v3/launches/${args.flightNumber}`).then(res => res.data);
      },
    },
    rockets: {
      type: new GraphQLList(RocketType),
      resolve(parent, args) {
        return axios.get('https://api.spacexdata.com/v3/rockets').then(res => res.data);
      },
    },
    rocket: {
      type: RocketType,
      args: {
        id: {
          type: GraphQLInt,
        },
      },
      resolve(parent, args) {
        return axios.get(`https://api.spacexdata.com/v3/rockets/${args.id}`).then(res => res.data);
      },
    },
  },
});

module.exports = new GraphQLSchema({
  query: RootQuery,
});
