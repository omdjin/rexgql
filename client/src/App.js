import React, { Component } from 'react';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import Launches from './components/Launches';
import Launche from './components/Launch';
import './App.css';
import logoImg from './spacex-logo.svg';

const client = new ApolloClient({
  uri: 'http://localhost:5000/graphql',
});

class App extends Component {
  render() {
    return (
      <ApolloProvider client={client}>
        <Router>
          <div className="App">
            <div className="text-center">
              <img src={logoImg} alt="SpaceX" style={{ width: 300, display: 'block', margin: 'auto' }} />
            </div>
            <Route exact path="/" component={Launches} />
            <Route exact path="/launch/:flightNumber" component={Launche} />
          </div>
        </Router>
      </ApolloProvider>
    );
  }
}

export default App;
