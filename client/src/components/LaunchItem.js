import React from 'react';
import cx from 'classnames';
import Moment from 'react-moment';
import { Link } from 'react-router-dom';

export default function LaunchItem({ launch: { flightNumber, missionName, launchDateLocal, isSuccess } }) {
  return (
    <div className="card card-body mb-3">
      <div className="row">
        <div className="col-md-9">
          <h4>
            Mission: <span className={cx({ 'text-success': isSuccess, 'text-danger': !isSuccess })}>{missionName}</span>
          </h4>
          <p>
            Date: <Moment format="YYYY-MM-DD HH:mm">{launchDateLocal}</Moment>
          </p>
        </div>
        <div className="col-md-3">
          <Link to={`/launch/${flightNumber}`} className="btn btn-secondary">
            Launch Details
          </Link>
        </div>
      </div>
    </div>
  );
}
