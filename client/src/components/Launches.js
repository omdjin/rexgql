import React, { Component, Fragment } from 'react';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';

import LaunchItem from './LaunchItem';
import MissionKey from './MissionKey';

const LAUNCH_QUERY = gql`
  query launchesQuery {
    launches {
      flightNumber
      missionName
      launchDateLocal
      isSuccess
    }
  }
`;

export class Launches extends Component {
  render() {
    return (
      <Fragment>
        <h1 className="display-4 my-3">Launches</h1>
        <MissionKey />
        <Query query={LAUNCH_QUERY}>
          {({ loading, error, data }) => {
            if (loading) {
              return <h4>Loading..</h4>;
            }

            if (error) {
              console.log(error);
            }

            return (
              <Fragment>
                {data.launches.map(launch => (
                  <LaunchItem key={launch.flightNumber} launch={launch} />
                ))}
              </Fragment>
            );
          }}
        </Query>
      </Fragment>
    );
  }
}

export default Launches;
