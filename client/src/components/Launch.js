import React, { Component, Fragment } from 'react';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import { Link } from 'react-router-dom';
import cx from 'classnames';

const LAUNCH_QUERY = gql`
  query LaunchQuery($flightNumber: Int!) {
    launch(flightNumber: $flightNumber) {
      flightNumber
      missionName
      launchYear
      isSuccess
      launchDateLocal
      rocket {
        rocketId
        rocketName
        rocketType
      }
    }
  }
`;

export class Launch extends Component {
  render() {
    let { flightNumber } = this.props.match.params;
    flightNumber = parseInt(flightNumber, 10);
    return (
      <Fragment>
        <Query query={LAUNCH_QUERY} variables={{ flightNumber }}>
          {({ loading, error, data }) => {
            if (loading) return <h4>Loading</h4>;

            if (error) console.log(error);

            const {
              missionName,
              flightNumber,
              launchYear,
              isSuccess,
              rocket: { rocketId, rocketName, rocketType },
            } = data.launch;

            console.log(data);
            return (
              <div>
                <h1 className="display-4 my-3">
                  <span className="text-dark">Mission:</span> {missionName}
                </h1>
                <h4>Launch Detail</h4>
                <ul className="list-group">
                  <li className="list-group-item">Flight Number: {flightNumber}</li>
                  <li className="list-group-item">Launch Year: {launchYear}</li>
                  <li className="list-group-item">
                    Launch Successful:{' '}
                    <span
                      className={cx({
                        'text-success': isSuccess,
                        'text-danger': !isSuccess,
                      })}
                    >
                      {isSuccess ? 'Yes' : 'No'}
                    </span>
                  </li>
                </ul>
                <h4 className="my-3">Rocket Details</h4>
                <ul className="list-group">
                  <li className="list-group-item">Rocket ID: {rocketId}</li>
                  <li className="list-group-item">Rocket Name: {rocketName}</li>
                  <li className="list-group-item">Rocket Type: {rocketType}</li>
                </ul>
                <hr />
                <Link to="/" className="btn btn-secondary">
                  Back
                </Link>
              </div>
            );
          }}
        </Query>
      </Fragment>
    );
  }
}

export default Launch;
